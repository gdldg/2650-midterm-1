// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    
    let year  = context.data.year;
    let model  = context.data.model;
    let make  = context.data.make;
    let mileage  = context.data.mileage;
    const length = 30;

    if(year < 1885 || context.data.year > 2020){
      console.log('year invalid');
      throw Error('Invalid year');
    }

    if(mileage <= 0){
      throw Error('Invalid mileage');
    }
    
    if(make === ''){
      throw Error('Invalid make, must not be empty');
    }
    
    if(model === ''){
      throw Error('Invalid model, must not be empty');
    }
    
    model = model.substring(0, length);
    make = make.substring(0, length);
    
    return context;
  };
}
